#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
    int size = toSort.size()     ;
    int min  = toSort[0]         ;
    int la                       ;
    int temp                     ;
    for(int j=0; j<size; j++)    {
        min  = toSort[j]         ;
        for(int i=j; i<size; i++){
            if(toSort[i]<=min)   {
                min = toSort[i]  ;
                la  = i          ;
                                 }
                                 }
        temp = toSort[j]         ;
        toSort[j] = min          ;
        toSort[la]= temp         ;
                                 }
                                 }

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
