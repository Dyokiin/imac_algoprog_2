#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
    int i = toSort.size();
    int temp = 0;

    for(int j=0;j<i;j++){
        for(int k=j;k<i;k++){
            if(toSort[k]<=toSort[j]){
                temp = toSort[j];
                toSort[j] = toSort[k];
                toSort[k] = temp;
            }
        }
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
