#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void insertionSort(Array& toSort){
	Array& sorted=w->newArray(toSort.size());

    int size = toSort.size();
    int ou;
    int temp;
    int j;

    for(int i=0;i<size;i++){
        for(int j=0;j<size;j++){
            if(toSort[i]>sorted[j]){
                ou = j;
                temp = toSort[i];
                break;
            }
        }
        sorted.insert(ou, temp);
    }
    for(int i=0;i<size;i++){
        sorted.insert(i, sorted[size-1]);
    }

    toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
