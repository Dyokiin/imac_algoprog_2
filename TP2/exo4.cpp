#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size)
{
    // stop statement = condition + return (return stop the function even if it does not return anything)
    if(size>1){

        Array& lowerArray = w->newArray(size);
        Array& greaterArray= w->newArray(size);
        int pivot;
        int lowerSize = 0, greaterSize = 0; // effectives sizes
        int temp;
        // split

        for(int i=1;i<size; i++){
            if(pivot >= toSort[i]){
                lowerArray[lowerSize]=toSort[i];
                lowerSize++;
            }else{
                greaterArray[greaterSize]=toSort[i];
                greaterSize++;
            }
        }

        // recursiv sort of lowerArray and greaterArray

        recursivQuickSort(greaterArray, greaterSize);
        recursivQuickSort(lowerArray, lowerSize);
        // merge

        bool putPivot = false;
        int j = 0;
        for(int i=0; i<size; i++){
            if(i<lowerSize){toSort[i]=lowerArray[i];}
            else{
                if(!putPivot){putPivot=true;toSort[i]=pivot;}
                else{toSort[i]=greaterArray[j];j++;}
            }
        }
    }else{return;}
}


void quickSort(Array& toSort){
    recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
