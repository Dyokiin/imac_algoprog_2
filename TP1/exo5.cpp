#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot
    float x = z.x;
    float y = z.y;
    float a = point.x;
    float b = point.y;

    Point next;
    next.x = pow(x,2) - pow(y,2) + a;
    next.y = 2*x*y+b;

    float moduleZ = sqrt(pow(x,2)+pow(y,2));

    if(n==0){
        return 0;
    } else {
        if(moduleZ>2){
            return n;
        } else {
            return isMandelbrot(next, n-1, point);
        }
    }
    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



